﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly PromoCodeDbContext _context;
        public EfRepository(PromoCodeDbContext context)
        {
            _context=context;
        }
        public async Task<IEnumerable<T>> GetAllAsync() => await _context.Set<T>().ToListAsync();
        public async Task<T> GetByIdAsync(Guid id) => await _context.Set<T>().FindAsync(id);

        public async Task<bool> AddAsync(T model)
        {
            await _context.AddAsync(model);
            return await _context.SaveChangesAsync() >0;
        }

        public async Task<bool> EditAsync(T model)
        {
            _context.Update(model);
            return await _context.SaveChangesAsync() >0;
        }

        public async Task<bool> DeleteAsync(Guid id)
        {
            var modelDb = await _context.Set<T>().FindAsync(id);
            if (modelDb != null)
                _context.Set<T>().Remove(modelDb);

            return await _context.SaveChangesAsync() >0;

        }
    }
}
