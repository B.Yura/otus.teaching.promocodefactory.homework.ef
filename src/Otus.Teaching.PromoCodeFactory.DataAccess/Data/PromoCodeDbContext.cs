﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;


namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class PromoCodeDbContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<CustomerPreference> CustomerPreferences { get; set; }
        public PromoCodeDbContext(DbContextOptions<PromoCodeDbContext> options)
            : base(options)
        {

            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<CustomerPreference>()
                    .HasKey(c => new { c.CustomerId, c.PreferenceId });
            builder.Entity<Customer>().HasIndex(x => x.Email).IsUnique();

            builder.Entity<PromoCode>().HasOne(x => x.Customer).WithMany(x => x.Promocodes).OnDelete(DeleteBehavior.Cascade);

            builder.Entity<Role>().HasData(FakeDataFactory.Roles);
            builder.Entity<Employee>().HasData(FakeDataFactory.Employees);
            builder.Entity<Preference>().HasData(FakeDataFactory.Preferences);
            builder.Entity<Customer>().HasData(FakeDataFactory.Customers);
            builder.Entity<CustomerPreference>().HasData(FakeDataFactory.CustomerPreferences);


        }
    }
}
