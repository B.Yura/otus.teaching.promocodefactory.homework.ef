﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> _promocodesRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<Employee> _employeeRepository;
        public PromocodesController(IRepository<PromoCode> promocodesRepository,
                                    IRepository<Preference> preferenceRepository,
                                    IRepository<Employee> employeeRepository)
        {
            _promocodesRepository=promocodesRepository;
            _preferenceRepository=preferenceRepository;
            _employeeRepository=employeeRepository;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            //TODO: Получить все промокоды 
            var promocodes = await _promocodesRepository.GetAllAsync();


            return Ok(promocodes.Select(x => new PromoCodeShortResponse
            {
                BeginDate = x.BeginDate.ToString("dd.MM.yyyy"),
                EndDate = x.EndDate.ToString("dd.MM.yyyy"),
                Code = x.Code,
                Id = x.Id,
                PartnerName = x.PartnerName,
                ServiceInfo = x.ServiceInfo
            }));
        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            //TODO: Создать промокод и выдать его клиентам с указанным предпочтением
            var preferense = (await _preferenceRepository.GetAllAsync())?.FirstOrDefault(x => x.Name.Contains(request.Preference));
            var customers = preferense?.CustomerPreferences?.Select(x => x.Customer);
            var employee = (await _employeeRepository.GetAllAsync())?.FirstOrDefault(x => $"{x.LastName}{x.FirstName}".Contains(request.PartnerName, StringComparison.OrdinalIgnoreCase));

            foreach (var customer in customers)
            {
                var promocode = new PromoCode
                {
                    Id = Guid.NewGuid(),
                    BeginDate = DateTime.Now,
                    EndDate = DateTime.Now.AddDays(7),
                    ServiceInfo = request.ServiceInfo,
                    PartnerName = request.PartnerName,
                    Code = request.PromoCode,
                    Preference = preferense,
                    Customer = customer,
                    PartnerManager = employee
                };
                await _promocodesRepository.AddAsync(promocode);
            }

            return Ok(true);

        }
    }
}