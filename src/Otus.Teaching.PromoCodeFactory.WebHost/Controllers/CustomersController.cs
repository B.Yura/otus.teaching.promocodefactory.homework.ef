﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _сustomerRepository;

        public CustomersController(IRepository<Customer> сustomerRepository)
        {
            _сustomerRepository=сustomerRepository;
        }
        /// <summary>
        /// Получить список всех клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet("all")]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
        {
            //TODO: Добавить получение списка клиентов
            var customers = await _сustomerRepository.GetAllAsync();
            if (customers == null)
                return NotFound();

            var response = customers.Select(x => new CustomerShortResponse
            {
                Id = x.Id,
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            });
            return Ok(response);
        }
        /// <summary>
        /// Получить клиента по его id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("byid/{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            //TODO: Добавить получение клиента вместе с выданными ему промокодами
            if (id == Guid.Empty)
                return BadRequest();

            var customer = await _сustomerRepository.GetByIdAsync(id);
            if (customer == null)
                return NotFound();

            var response = new CustomerResponse
            {
                Id = customer.Id,
                Email = customer.Email,
                LastName = customer.LastName,
                FirstName = customer.FirstName,
                PromoCodes = customer.Promocodes.Select(x => new PromoCodeShortResponse
                {
                    BeginDate = x.BeginDate.ToString(),
                    Code = x.Code,
                    EndDate = x.EndDate.ToString(),
                    Id = x.Id,
                    PartnerName = x.PartnerName,
                    ServiceInfo = x.ServiceInfo
                }).ToList()
            };
            return Ok(response);
        }
        /// <summary>
        /// Добавить нового клиента
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("add")]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            //TODO: Добавить создание нового клиента вместе с его предпочтениями
            if (request is null)
                return BadRequest();
            var customer = new Customer
            {
                Id = Guid.NewGuid(),
                FirstName= request.FirstName,
                LastName= request.LastName,
                Email= request.Email,
                CustomerPreferences = request.PreferenceIds.Select(x => new CustomerPreference
                {
                    PreferenceId = x
                }).ToList()
            };
            return Ok(await _сustomerRepository.AddAsync(customer));


        }
        /// <summary>
        /// Редактировать клиента
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("edit/{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            //TODO: Обновить данные клиента вместе с его предпочтениями
            if (id  == Guid.Empty)
                return BadRequest();

            var customer = new Customer
            {
                Id = id,
                FirstName= request.FirstName,
                LastName= request.LastName,
                Email= request.Email,
                CustomerPreferences = request.PreferenceIds.Select(x => new CustomerPreference
                {
                    PreferenceId = x
                }).ToList()
            };

            return Ok(await _сustomerRepository.EditAsync(customer));

        }
        /// <summary>
        /// Удалить клиента
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("delete/{id}")]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            //TODO: Удаление клиента вместе с выданными ему промокодами
            return Ok(await _сustomerRepository.DeleteAsync(id));

        }
    }
}