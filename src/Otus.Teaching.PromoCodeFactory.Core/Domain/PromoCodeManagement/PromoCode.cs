﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime;
using System.Text.Json.Serialization;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class PromoCode : BaseEntity
    {
        [MaxLength(200)]
        public string Code { get; set; }
        [MaxLength(200)]
        public string ServiceInfo { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }
        [MaxLength(200)]
        public string PartnerName { get; set; }

        [JsonIgnore] public virtual Employee PartnerManager { get; set; }
        [JsonIgnore] public virtual Preference Preference { get; set; }
        [JsonIgnore] public virtual Customer Customer { get; set; }
    }
}