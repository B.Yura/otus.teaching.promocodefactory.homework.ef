﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer : BaseEntity
    {
        [MaxLength(200)]
        public string FirstName { get; set; }
        [MaxLength(200)]
        public string LastName { get; set; }
        public string FullName => $"{FirstName} {LastName}";
        [MaxLength(200)]
        public string Email { get; set; }

        //TODO: Списки Preferences и Promocodes 
        public virtual ICollection<CustomerPreference> CustomerPreferences { get; set; }
        public virtual ICollection<PromoCode> Promocodes { get; set; }
    }
}